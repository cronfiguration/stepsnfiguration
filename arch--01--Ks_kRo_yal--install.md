# arch Ks_kRo_yal install 
![](images/2023-05-05-19-42-52.png)

ping goog le  
![](images/2023-05-05-19-44-36.png)

iwctl  
![](images/2023-05-05-19-44-49.png)

device list  
![](images/2023-05-05-19-45-09.png)

device wlan show  
![](images/2023-05-05-19-45-25.png)

wlan config  
![](images/2023-05-05-19-45-55.png)

initialize pacman packages  
![](images/2023-05-05-19-46-37.png)

lsblk  
![](images/2023-05-05-19-46-54.png)

format disk x expert mode   
![](images/2023-05-05-19-47-21.png)
![](images/2023-05-05-19-47-28.png)

z option  
![](images/2023-05-05-19-47-54.png)

blank out mbr  
![](images/2023-05-05-19-48-12.png)

arch install  
![](images/2023-05-05-19-48-37.png)