# nixos Step_hensTe_chTa_lks install minimal
setfont  
![](images/2023-05-03-19-42-55.png)

set networking  
![](images/2023-05-03-19-43-43.png)

become root  
![](images/2023-05-03-19-53-04.png)

search disks  
![](images/2023-05-03-19-53-32.png)

format disks  
![](images/2023-05-03-19-53-55.png)

new partition n, +600M at the end free, efi code ef00  
![](images/2023-05-03-19-54-54.png)

root partition - all defaults to build the rest of the disk   
![](images/2023-05-03-19-55-34.png)

w write partition  
![](images/2023-05-03-19-56-19.png)

y overwrite partition table  
![](images/2023-05-03-19-56-43.png)

format partition  
![](images/2023-05-03-19-58-43.png)

mount nixos partition  
![](images/2023-05-03-20-55-52.png)

mkdir for efi partition  
![](images/2023-05-03-20-56-19.png)

mount boot efi partition  
![](images/2023-05-03-20-56-51.png)

generate nix config  
![](images/2023-05-03-20-57-23.png)

open config in nano  
![](images/2023-05-03-20-57-44.png)

uncomment networking  
![](images/2023-05-03-20-58-01.png)
uncomment networkmanager  
![](images/2023-05-03-20-58-26.png)
uncomment timezone  
![](images/2023-05-03-20-58-42.png)
enable openssh  
![](images/2023-05-03-20-58-58.png)

uncomment firewall and sysConfiguration  
![](images/2023-05-03-20-59-36.png)
system state version leave default  
![](images/2023-05-03-21-00-10.png)

run install  
![](images/2023-05-03-21-00-45.png)
set password at the end  
![](images/2023-05-03-21-01-10.png)