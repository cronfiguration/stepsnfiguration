# Di_str_oTu_be graphical nixos

vim ne radi out of the box  
![](images/2023-05-05-18-50-49.png)

config  
![](images/2023-05-05-18-54-58.png)

edit packages  
![](images/2023-05-05-18-56-18.png)

edit kdenlive  
![](images/2023-05-05-18-56-51.png)

rebuild nixos  
![](images/2023-05-05-18-58-27.png)