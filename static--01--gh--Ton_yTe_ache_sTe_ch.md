# static gh Ton_yTe_ach_esTe_ch

gh repo  
![](images/2023-05-19-16-18-44.png)
![](images/2023-05-19-16-18-56.png)

settings scroll to gh pages  
![](images/2023-05-19-16-19-23.png)
![](images/2023-05-19-16-19-36.png)

test browser  
![](images/2023-05-19-16-19-59.png)

readme.md example  
![](images/2023-05-19-16-24-54.png)

choose a theme settings  
![](images/2023-05-19-16-25-28.png)
![](images/2023-05-19-16-25-38.png)

select theme from marketplace  
![](images/2023-05-19-16-26-24.png)

brisanje tako sto obrises _config.yml i commitas  
![](images/2023-05-19-16-27-17.png)
![](images/2023-05-19-16-27-28.png)

upload static theme na add files  
![](images/2023-05-19-16-28-09.png)

drag files  
![](images/2023-05-19-16-28-21.png)
![](images/2023-05-19-16-28-40.png)

processing after drag/drop  
![](images/2023-05-19-16-29-01.png)

treba upisat indx.html u adresu i nakon nekog vremena ce biti i na root adresi github.io  
![](images/2023-05-19-16-30-04.png)

za subdomene, radimo novi repo  
![](images/2023-05-19-16-30-46.png)
![](images/2023-05-19-16-31-16.png)

samo dodamo readme i u settings imamo ggh pages  
![](images/2023-05-19-16-31-44.png)

dodatne teme jekyll  
![](images/2023-05-19-16-32-52.png)